#ifndef LINKAGE_MODEL_HPP_
#define LINKAGE_MODEL_HPP_

#include <numbers>

#include <MAiNGO.h>

enum OptVarLayout : size_t {
  AX,         // x-position of first node
  AY,         // y-position of first node
  ANGLE,      // angle between x-axis and ground link
  L_AB,       // input link
  L_AD,       // ground link
  L_BC,       // connector
  L_CD,       // output link
  COUPLER_N,  // normal distance of coupler to link BC
  COUPLER_T,  // tangential distance of coupler to link BC
  NUM_OPT_VARS,
};

enum : size_t { X, Y };

template <typename T>
using Point = std::array<T, 2>;

// -------------------------------------------------------------------------------------------------
template <typename T1, typename T2>
auto dist_sqr(const Point<T1>& p1, const Point<T2>& p2) noexcept {
  return pow(p1[X] - p2[X], 2) + pow(p1[Y] - p2[Y], 2);
}

template <typename T1, typename T2>
auto dist(const Point<T1>& p1, const Point<T2>& p2) noexcept {
  return sqrt(dist_sqr(p1, p2));
}

// -------------------------------------------------------------------------------------------------
class LinkageModel : public maingo::MAiNGOmodel {
  static constexpr double eps = 1e-2;

  std::vector<Point<double>> m_fit_points;

  // -----------------------------------------------------------------------------------------------
  static auto theta_idx(size_t fit_pnt_idx) noexcept -> size_t {
    return NUM_OPT_VARS + 3 * fit_pnt_idx + 0;
  }
  static auto C_x_idx(size_t fit_pnt_idx) noexcept -> size_t {
    return NUM_OPT_VARS + 3 * fit_pnt_idx + 1;
  }
  static auto C_y_idx(size_t fit_pnt_idx) noexcept -> size_t {
    return NUM_OPT_VARS + 3 * fit_pnt_idx + 2;
  }

 public:
  // -----------------------------------------------------------------------------------------------
  LinkageModel(std::vector<Point<double>> fit_points) noexcept
      : m_fit_points(std::move(fit_points)) {}

  auto num_opt_vars() const noexcept -> size_t { return NUM_OPT_VARS + 3 * m_fit_points.size(); }

  // -----------------------------------------------------------------------------------------------
  auto evaluate(const std::vector<Var>& opt_vars) -> maingo::EvaluationContainer override {
    using namespace std::string_literals;

    assert(opt_vars.size() == num_opt_vars());
    const Point<Var> A   = {opt_vars[AX], opt_vars[AY]};
    const Var& angle     = opt_vars[ANGLE];
    const Var& l_ab      = opt_vars[L_AB];
    const Var& l_ad      = opt_vars[L_AD];
    const Var& l_bc      = opt_vars[L_BC];
    const Var& l_cd      = opt_vars[L_CD];
    const Var& coupler_n = opt_vars[COUPLER_N];
    const Var& coupler_t = opt_vars[COUPLER_T];

    const Point<Var> D = {A[X] + cos(angle) * l_ad, A[Y] + sin(angle) * l_ad};

    maingo::EvaluationContainer result;

    Var objective = 0.0;
    for (size_t fit_idx = 0; fit_idx < m_fit_points.size(); ++fit_idx) {
      const auto& fit_point = m_fit_points[fit_idx];
      const Var& theta      = opt_vars[theta_idx(fit_idx)];
      const Point<Var> B    = {A[X] + cos(theta) * l_ab, A[Y] + sin(theta) * l_ab};
      const Point<Var> C    = {opt_vars[C_x_idx(fit_idx)], opt_vars[C_y_idx(fit_idx)]};

      result.eq.push_back(dist(B, C) - l_bc,
                          "dist(B, C) = lBC ("s + std::to_string(fit_idx) + ")"s);
      result.eq.push_back(dist(C, D) - l_cd,
                          "dist(C, D) = lCD ("s + std::to_string(fit_idx) + ")"s);

      // TODO: Is this inequality needes?
      // result.ineq.push_back(-dist(B, C) + eps,
      //                       "dist(B, C) > 0 ("s + std::to_string(fit_idx) + ")"s);

      // IDEA: To make sure that we don't flip over, assert that the angle between BC and CD is
      // below 180°, i.e. C is on the correct side of the line BD
      const Point<Var> BC        = {C[X] - B[X], C[Y] - B[Y]};
      const Point<Var> normal_BC = {-BC[Y], BC[X]};
      const Point<Var> BD        = {D[X] - B[X], D[Y] - B[Y]};
      const Point<Var> normal_BD = {-BD[Y], BD[X]};

      const Var side = normal_BD[X] * BC[X] + normal_BD[Y] * BC[Y];
      result.ineq.push_back(-side, "C (" + std::to_string(fit_idx) + ") on correct side of BD");

      // NOTE: Both coupler_t and coupler_n are relative to the length lBC
      const Point<Var> E = {B[X] + coupler_t * BC[X] + coupler_n * normal_BC[X],
                            B[Y] + coupler_t * BC[Y] + coupler_n * normal_BC[Y]};

      result.output.push_back(
          maingo::OutputVariable("E_x (" + std::to_string(fit_idx) + ")", E[X]));
      result.output.push_back(
          maingo::OutputVariable("E_y (" + std::to_string(fit_idx) + ")", E[Y]));
      objective += dist_sqr(fit_point, E);
    }
    result.objective = objective;

    // Inequalities (<=0):
    const Var T1 = l_ad + l_bc - l_ab - l_cd;
    const Var T2 = l_cd + l_ad - l_ab - l_bc;
    const Var T3 = l_cd + l_bc - l_ab - l_ad;

    //     T1 >= eps
    // <=> 0  >= eps - T1
    result.ineq.push_back(-T1 + eps, "T1 >= eps");
    result.ineq.push_back(-T2 + eps, "T2 >= eps");
    result.ineq.push_back(-T3 + eps, "T3 >= eps");

    result.output.push_back(maingo::OutputVariable("Value of T1", T1));
    result.output.push_back(maingo::OutputVariable("Value of T2", T2));
    result.output.push_back(maingo::OutputVariable("Value of T3", T3));

    return result;
  }

  // -----------------------------------------------------------------------------------------------
  auto get_variables() -> std::vector<maingo::OptimizationVariable> override {
    std::vector<maingo::OptimizationVariable> opt_vars(
        num_opt_vars(), maingo::OptimizationVariable(maingo::Bounds(0.0, 0.0)));

    opt_vars[AX] =
        maingo::OptimizationVariable(maingo::Bounds(0.0, 1.0), maingo::VT_CONTINUOUS, "Ax");
    opt_vars[AY] =
        maingo::OptimizationVariable(maingo::Bounds(0.0, 1.0), maingo::VT_CONTINUOUS, "Ay");
    opt_vars[ANGLE] = maingo::OptimizationVariable(
        maingo::Bounds(-std::numbers::pi, std::numbers::pi), maingo::VT_CONTINUOUS, "angle");
    opt_vars[L_AB] =
        maingo::OptimizationVariable(maingo::Bounds(eps, 2.0), maingo::VT_CONTINUOUS, "lAB");
    opt_vars[L_AD] =
        maingo::OptimizationVariable(maingo::Bounds(eps, 2.0), maingo::VT_CONTINUOUS, "lAD");
    opt_vars[L_BC] =
        maingo::OptimizationVariable(maingo::Bounds(eps, 2.0), maingo::VT_CONTINUOUS, "lBC");
    opt_vars[L_CD] =
        maingo::OptimizationVariable(maingo::Bounds(eps, 2.0), maingo::VT_CONTINUOUS, "lCD");
    opt_vars[COUPLER_N] =
        maingo::OptimizationVariable(maingo::Bounds(-2.0, 2.0), maingo::VT_CONTINUOUS, "couplerN");
    opt_vars[COUPLER_T] =
        maingo::OptimizationVariable(maingo::Bounds(-2.0, 2.0), maingo::VT_CONTINUOUS, "couplerT");

    for (size_t fit_idx = 0; fit_idx < m_fit_points.size(); ++fit_idx) {
      opt_vars[theta_idx(fit_idx)] =
          maingo::OptimizationVariable(maingo::Bounds(-std::numbers::pi, std::numbers::pi),
                                       maingo::VT_CONTINUOUS,
                                       "theta (" + std::to_string(fit_idx) + ")");
      opt_vars[C_x_idx(fit_idx)] = maingo::OptimizationVariable(
          maingo::Bounds(-5.0, 5.0), maingo::VT_CONTINUOUS, "Cx (" + std::to_string(fit_idx) + ")");
      opt_vars[C_y_idx(fit_idx)] = maingo::OptimizationVariable(
          maingo::Bounds(-5.0, 5.0), maingo::VT_CONTINUOUS, "Cy (" + std::to_string(fit_idx) + ")");
    }

    return opt_vars;
  }

  // -----------------------------------------------------------------------------------------------
  auto get_initial_point() -> std::vector<double> override {
    std::vector<double> init_guess(num_opt_vars(), 0.0);
    init_guess[AX]        = 0.0;
    init_guess[AY]        = 0.0;
    init_guess[ANGLE]     = 0.0;
    init_guess[L_AB]      = 0.66;
    init_guess[L_AD]      = 1.0;
    init_guess[L_BC]      = 1.0;
    init_guess[L_CD]      = 1.0;
    init_guess[COUPLER_N] = 0.5;
    init_guess[COUPLER_T] = 0.25;

    return init_guess;
  }
};

#endif  // LINKAGE_MODEL_HPP_
