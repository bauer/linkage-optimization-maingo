#include <iostream>
#include <memory>
#include <optional>

#include "LinkageModel.hpp"

// ------------------------------------------------------------------------------------------------
struct Linkage {
  double Ax;         // x-position of first node
  double Ay;         // y-position of first node
  double angle;      // angle between x-axis and ground link
  double l_AB;       // input link
  double l_AD;       // ground link
  double l_BC;       // connector
  double l_CD;       // output link
  double coupler_n;  // normal distance of coupler to link BC
  double coupler_t;  // tangential distance of coupler to link BC
};

auto operator<<(std::ostream& out, const Linkage& linkage) noexcept -> std::ostream& {
  out << "{\n";
  out << "\t.Ax        = " << linkage.Ax << ",\n";
  out << "\t.Ay        = " << linkage.Ay << ",\n";
  out << "\t.angle     = " << linkage.angle << ",\n";
  out << "\t.l_AB      = " << linkage.l_AB << ",\n";
  out << "\t.l_AD      = " << linkage.l_AD << ",\n";
  out << "\t.l_BC      = " << linkage.l_BC << ",\n";
  out << "\t.l_CD      = " << linkage.l_CD << ",\n";
  out << "\t.coupler_n = " << linkage.coupler_n << ",\n";
  out << "\t.coupler_t = " << linkage.coupler_t << ",\n";
  out << '}';
  return out;
}

struct Solution {
  maingo::RETCODE status;
  double obj_value;
  Linkage linkage;
};

// ------------------------------------------------------------------------------------------------
auto find_local_optimum(const std::vector<Point<double>>& fit_points) noexcept
    -> std::optional<Solution> {
  std::shared_ptr<LinkageModel> model;
  std::unique_ptr<maingo::MAiNGO> solver;
  try {
    model  = std::make_shared<LinkageModel>(fit_points);
    solver = std::make_unique<maingo::MAiNGO>(model);

    solver->set_option("maxTime", 2 * 60 * 60);  // Max. time is 2h

    solver->set_option("PRE_pureMultistart", 1);     // Only multistart, no branch and bound
    solver->set_option("PRE_maxLocalSearches", 10);  // Run 10 local optimizers

    solver->set_option("loggingDestination", maingo::LOGGING_DESTINATION::LOGGING_FILE);
    solver->set_log_file_name("maingo_only_ms.log");

    solver->set_result_file_name("maingo_only_ms_result.txt");
  } catch (const std::exception& e) {
    std::cerr << "Error when initializing MAiNGO model:\n" << e.what() << '\n';
    return std::nullopt;
  }

  maingo::RETCODE retcode;
  try {
    retcode = solver->solve();
  } catch (const std::exception& e) {
    std::cerr << "Error when solving problem:\n" << e.what() << '\n';
    return std::nullopt;
  }

  switch (retcode) {
    case maingo::INFEASIBLE:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "INFEASIBLE.\n";
      return std::nullopt;
    case maingo::NO_FEASIBLE_POINT_FOUND:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "NO_FEASIBLE_POINT_FOUND.\n";
      return std::nullopt;
    case maingo::BOUND_TARGETS:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "BOUND_TARGETS.\n";
      return std::nullopt;
    case maingo::NOT_SOLVED_YET:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "NOT_SOLVED_YET.\n";
      return std::nullopt;
    case maingo::JUST_A_WORKER_DONT_ASK_ME:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "JUST_A_WORKER_DONT_ASK_ME.\n";
      return std::nullopt;
    case maingo::FEASIBLE_POINT:
    case maingo::GLOBALLY_OPTIMAL:
      try {
        const std::vector<double> solution = solver->get_solution_point();

        return Solution{
            .status    = retcode,
            .obj_value = solver->get_objective_value(),
            .linkage =
                {
                    .Ax        = solution[AX],
                    .Ay        = solution[AY],
                    .angle     = solution[ANGLE],
                    .l_AB      = solution[L_AB],
                    .l_AD      = solution[L_AD],
                    .l_BC      = solution[L_BC],
                    .l_CD      = solution[L_CD],
                    .coupler_n = solution[COUPLER_N],
                    .coupler_t = solution[COUPLER_T],
                },
        };
      } catch (const std::exception& e) {
        std::cerr << "Error when accessing final solution: " << e.what() << '\n';
        return std::nullopt;
      }
  }
  assert(false && "unreachable");
  std::exit(1);
}

// ------------------------------------------------------------------------------------------------
auto find_global_optimum(const std::vector<Point<double>>& fit_points) noexcept
    -> std::optional<Solution> {
  std::shared_ptr<LinkageModel> model;
  std::unique_ptr<maingo::MAiNGO> solver;
  try {
    model  = std::make_shared<LinkageModel>(fit_points);
    solver = std::make_unique<maingo::MAiNGO>(model);

    solver->set_option("maxTime", 2 * 60 * 60);  // Max. time is 2h

    solver->set_option("loggingDestination", maingo::LOGGING_DESTINATION::LOGGING_FILE);
    solver->set_log_file_name("maingo_bnb.log");

    solver->set_result_file_name("maingo_bnb_result.txt");
  } catch (const std::exception& e) {
    std::cerr << "Error when initializing MAiNGO model:\n" << e.what() << '\n';
    return std::nullopt;
  }

  maingo::RETCODE retcode;
  try {
    retcode = solver->solve();
  } catch (const std::exception& e) {
    std::cerr << "Error when solving problem:\n" << e.what() << '\n';
    return std::nullopt;
  }

  switch (retcode) {
    case maingo::INFEASIBLE:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "INFEASIBLE.\n";
      return std::nullopt;
    case maingo::NO_FEASIBLE_POINT_FOUND:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "NO_FEASIBLE_POINT_FOUND.\n";
      return std::nullopt;
    case maingo::BOUND_TARGETS:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "BOUND_TARGETS.\n";
      return std::nullopt;
    case maingo::NOT_SOLVED_YET:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "NOT_SOLVED_YET.\n";
      return std::nullopt;
    case maingo::JUST_A_WORKER_DONT_ASK_ME:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "JUST_A_WORKER_DONT_ASK_ME.\n";
      return std::nullopt;
    case maingo::FEASIBLE_POINT:
      std::cerr << "Warn: MAiNGO did not find a globally optimal point, exited with status "
                   "FEASIBLE_POINT.\n";
    case maingo::GLOBALLY_OPTIMAL:
      try {
        const std::vector<double> solution = solver->get_solution_point();

        return Solution{
            .status    = retcode,
            .obj_value = solver->get_objective_value(),
            .linkage =
                {
                    .Ax        = solution[AX],
                    .Ay        = solution[AY],
                    .angle     = solution[ANGLE],
                    .l_AB      = solution[L_AB],
                    .l_AD      = solution[L_AD],
                    .l_BC      = solution[L_BC],
                    .l_CD      = solution[L_CD],
                    .coupler_n = solution[COUPLER_N],
                    .coupler_t = solution[COUPLER_T],
                },
        };
      } catch (const std::exception& e) {
        std::cerr << "Error when accessing final solution: " << e.what() << '\n';
        return std::nullopt;
      }
  }
  assert(false && "unreachable");
  std::exit(1);
}

// -------------------------------------------------------------------------------------------------
auto main() -> int {
  // const std::vector<Point<double>> fit_points = {
  //     {-0.3, 0.0},
  //     {0.4, 0.5},
  //     {0.5, 1.2},
  //     {-0.5, 0.2},
  //     {0.2, 0.3},
  //     {0.6, 1.4},
  //     {0.7, 1.2},
  //     {0.1, 0.2},
  //     {-0.1, 0.1},
  // };
  const std::vector<Point<double>> fit_points = {
      {-0.3, 0.0},
      {0.4, 0.5},
      {0.5, 1.2},
      {-0.5, 0.2},
      {0.2, 0.3},
      {0.6, 1.4},
      {0.7, 1.2},
  };

  {
    std::cout << "Run local optimizer...\n";
    const auto local_solution = find_local_optimum(fit_points);
    if (!local_solution.has_value()) {
      std::cerr << "Did not find optimum.";
      return 1;
    }

    std::cout << "Objective value: " << local_solution->obj_value << '\n';
    std::cout << "Optimal point: " << local_solution->linkage << '\n';
  }

  std::cout << "\n--------------------------------------------------\n\n";

  {
    std::cout << "Run global optimizer...\n";
    const auto global_solution = find_global_optimum(fit_points);
    if (!global_solution.has_value()) {
      std::cerr << "Did not find optimum.";
      return 1;
    }

    switch (global_solution->status) {
      case maingo::GLOBALLY_OPTIMAL: std::cout << "Found global optimum\n"; break;
      case maingo::FEASIBLE_POINT: std::cout << "Found feasible point\n"; break;
      default: assert(false && "unreachable"); std::exit(1);
    }

    std::cout << "Objective value: " << global_solution->obj_value << '\n';
    std::cout << "Optimal point: " << global_solution->linkage << '\n';
  }
}
