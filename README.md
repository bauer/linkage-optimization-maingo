# Optimizing the four-bar linkage problem
Optimizing the four-bar linkage problem using the deterministic global optimizer [MAiNGO](http://permalink.avt.rwth-aachen.de/?id=729717).

## Getting started

```console
$ git submodule update --init --recursive -j 1
$ cmake -Bbuild -DCMAKE_BUILD_TYPE=Release
$ cd build
$ make -j
$ ./linkage_gui
```