// Dear ImGui: standalone example application for SDL2 + SDL_Renderer
// (SDL is a cross-platform general purpose library for handling windows,
// inputs, OpenGL/Vulkan/Metal graphics context creation, etc.)

// Learn about Dear ImGui:
// - FAQ                  https://dearimgui.com/faq
// - Getting Started      https://dearimgui.com/getting-started
// - Documentation        https://dearimgui.com/docs (same as your local docs/
// folder).
// - Introduction, links and more at the top of imgui.cpp

// Important to understand: SDL_Renderer is an _optional_ component of SDL2.
// For a multi-platform app consider using e.g. SDL+DirectX on Windows and
// SDL+OpenGL on Linux/OSX.

#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <mutex>
#include <optional>
#include <thread>
#include <vector>

#include "imgui.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_sdlrenderer2.h"
#include <SDL.h>

#include "implot.h"

#include "LinkageModel.hpp"

#if !SDL_VERSION_ATLEAST(2, 0, 17)
#error This backend requires SDL 2.0.17+ because of SDL_RenderGeometry() function
#endif

std::mutex g_trajectory_lock{};  // NOLINT

constexpr auto normSq(const Point<double>& p) -> double { return (p[0] * p[0] + p[1] * p[1]); }
constexpr auto norm2(const Point<double>& p) -> double { return sqrt(normSq(p)); }

struct Linkage {
  Point<double> A;
  // all other parameters as float, as this is easier for ImGui
  float angle;  // angle between x-axis and ground link
  float lAB;    // input link
  float lAD;    // ground link
  float lBC;    // connector
  float lCD;    // output link
  float couplerN;
  float couplerT;

  // trajectories as double, as this is easier for ImPlot
  std::vector<double> C_traj_x;
  std::vector<double> C_traj_y;

  std::vector<double> E_traj_x;
  std::vector<double> E_traj_y;

  auto operator==(const Linkage& l) -> bool {
    return std::tie(A[0], A[1], angle, lAB, lAD, lBC, lCD, couplerN, couplerT) ==
           std::tie(l.A[0], l.A[1], l.angle, l.lAB, l.lAD, l.lBC, l.lCD, l.couplerN, l.couplerT);
  }

  auto operator!=(const Linkage& l) -> bool { return !(*this == l); }

  // all computation as double
  void add_angle(double theta) {
    // TODO: The trajectory is not always correct, point C might be on the incorrect side of line
    // BD.

    Point<double> B;
    Point<double> C;
    Point<double> D;
    Point<double> E;

    B[0] = A[0] + cos(theta) * lAB;
    B[1] = A[1] + sin(theta) * lAB;

    D[0] = A[0] + cos(angle) * lAD;
    D[1] = A[1] + sin(angle) * lAD;

    std::array<std::array<double, 2>, 2> Jinv{};
    Point<double> f = {1.0, 1.0};
    int it          = 0;
    if (!C_traj_x.empty()) {
      C[0] = C_traj_x[C_traj_x.size() - 1];
      C[1] = C_traj_y[C_traj_y.size() - 1];
    } else {
      C[0] = 0.45235342;
      C[1] = 0.54364576;
    }
    while (f[0] * f[0] + f[1] * f[1] > 1e-9 && it < 150) {
      it++;
      f[0] = pow(C[0] - B[0], 2) + pow(C[1] - B[1], 2) - pow(lBC, 2);
      f[1] = pow(C[0] - D[0], 2) + pow(C[1] - D[1], 2) - pow(lCD, 2);

      double det = (2 * B[0] * C[1] - 2 * B[0] * D[1] - 2 * B[1] * C[0] + 2 * B[1] * D[0] +
                    2 * C[0] * D[1] - 2 * C[1] * D[0]);
      Jinv[0][0] = (-C[1] + D[1]) / det;
      Jinv[0][1] = (-B[1] + C[1]) / det;
      Jinv[1][0] = (C[0] - D[0]) / det;
      Jinv[1][1] = (B[0] - C[0]) / det;

      C[0] -= Jinv[0][0] * f[0] + Jinv[0][1] * f[1];
      C[1] -= Jinv[1][0] * f[0] + Jinv[1][1] * f[1];
    }

    Point<double> BC     = {C[0] - B[0], C[1] - B[1]};
    double norm          = sqrt(BC[0] * BC[0] + BC[1] * BC[1]);
    Point<double> normal = {-BC[1] / norm, BC[0] / norm};

    E[0] = B[0] + couplerT * BC[0] + couplerN * normal[0] * lBC;
    E[1] = B[1] + couplerT * BC[1] + couplerN * normal[1] * lBC;

    C_traj_x.push_back(C[0]);
    C_traj_y.push_back(C[1]);

    E_traj_x.push_back(E[0]);
    E_traj_y.push_back(E[1]);

    // std::cout << theta << " " << C[0] << " " << C[1] << " " << f[0] << " " << f[1] << " it: " <<
    // it << std::endl;
  }
};

/*struct LinkageOptimizer {
  virtual optimize(const Linkage &link, std::vector<std::array<double,2>> optPoints) = 0;
};*/

/*struct MultistartOptimizer : public LinkageOptimizer {
  void NAG_CALL objfun(Integer *mode, Integer n, const double *x,
                                  double *objf, double *objgrd, Integer nstate,
                                  Nag_Comm *comm);
  void NAG_CALL confun(Integer *mode, Integer ncnln, Integer n,
                                     Integer tdcjsl, const Integer *needc,
                                     const double *x, double *c, double *cjsl,
                                     Integer nstate, Nag_Comm *comm);
  void NAG_CALL start(Integer npts, double quas[], Integer n,
                             Nag_Boolean repeat, const double bl[],
                             const double bu[], Nag_Comm *comm, Integer *mode);
};*/

void computeLinkageTask(Linkage& link, bool& done) {
  std::scoped_lock lock(g_trajectory_lock);

  link.C_traj_x.clear();
  link.C_traj_y.clear();
  link.E_traj_x.clear();
  link.E_traj_y.clear();

  for (int theta = 0; theta <= 360; theta++) {
    link.add_angle(theta * M_PI / 180.0);
  }
  done = true;
}

template <typename T>
auto softMin(const std::vector<T>& x, double alpha = 5.0) -> T {
  T s1 = 0.0;
  T s2 = 0.0;
  for (const auto& v : x) {
    T t = exp(-alpha * v);
    s1 += v * t;
    s2 += t;
  }
  return s1 / s2;
}

template <typename T>
auto softMax(const std::vector<T>& x, double alpha = 2.0) -> T {
  T s1 = 0.0;
  T s2 = 0.0;
  for (const auto& v : x) {
    T t = exp(alpha * v);
    s1 += v * t;
    s2 += t;
  }
  return s1 / s2;
}

template <typename T>
auto eval_f(const Linkage& link,
            const std::vector<Point<double>>& optPoints,
            float alpha = 5.0) -> T {
  std::scoped_lock lock(g_trajectory_lock);

  const std::vector<T>& traj_x = link.E_traj_x;
  const std::vector<T>& traj_y = link.E_traj_y;

  T obj_value = 0.0;

  // find approx min distance to all reference points
  for (const Point<double>& opt_point : optPoints) {
    std::vector<T> diff(traj_x.size());
    for (std::size_t j = 0; j < traj_x.size(); j++) {
      diff[j] = -1.0 + pow(traj_x[j] - opt_point[0], 2) + pow(traj_y[j] - opt_point[1], 2);
    }
    obj_value += softMin(diff, alpha) + 1.0;
  }

  return obj_value;
}

auto find_optimum(const std::vector<Point<double>>& fit_points) noexcept -> std::optional<Linkage> {
  std::shared_ptr<LinkageModel> model;
  std::unique_ptr<maingo::MAiNGO> solver;
  try {
    model  = std::make_shared<LinkageModel>(fit_points);
    solver = std::make_unique<maingo::MAiNGO>(model);

    solver->set_option("loggingDestination", 0);
    solver->set_option("maxTime", 15);
  } catch (const std::exception& e) {
    std::cerr << "Error when initializing MAiNGO model:\n" << e.what() << '\n';
    return std::nullopt;
  }

  maingo::RETCODE retcode;
  try {
    retcode = solver->solve();
  } catch (const std::exception& e) {
    std::cerr << "Error when solving problem:\n" << e.what() << '\n';
    return std::nullopt;
  }

  switch (retcode) {
    case maingo::INFEASIBLE:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "INFEASIBLE.\n";
      return std::nullopt;
    case maingo::NO_FEASIBLE_POINT_FOUND:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "NO_FEASIBLE_POINT_FOUND.\n";
      return std::nullopt;
    case maingo::BOUND_TARGETS:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "BOUND_TARGETS.\n";
      return std::nullopt;
    case maingo::NOT_SOLVED_YET:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "NOT_SOLVED_YET.\n";
      return std::nullopt;
    case maingo::JUST_A_WORKER_DONT_ASK_ME:
      std::cerr << "Error: MAiNGO did not find a globally optimal point, exited with status "
                   "JUST_A_WORKER_DONT_ASK_ME.\n";
      return std::nullopt;
    case maingo::FEASIBLE_POINT:
      std::cerr << "Warning: MAiNGO did not find a globally optimal point, exited with status "
                   "FEASIBLE_POINT.\n";
      std::cerr << "Continue with feasible point.\n";
    case maingo::GLOBALLY_OPTIMAL:
      try {
        const std::vector<double> solution = solver->get_solution_point();

        return Linkage{
            .A        = {solution[AX], solution[AY]},
            .angle    = static_cast<float>(solution[ANGLE]),
            .lAB      = static_cast<float>(solution[L_AB]),
            .lAD      = static_cast<float>(solution[L_AD]),
            .lBC      = static_cast<float>(solution[L_BC]),
            .lCD      = static_cast<float>(solution[L_CD]),
            .couplerN = static_cast<float>(solution[COUPLER_N]),
            .couplerT = static_cast<float>(solution[COUPLER_T]),
            .C_traj_x = {},
            .C_traj_y = {},
            .E_traj_x = {},
            .E_traj_y = {},
        };
      } catch (const std::exception& e) {
        std::cerr << "Error when accessing final solution: " << e.what() << '\n';
        return std::nullopt;
      }
  }
  assert(false && "unreachable");
  std::exit(1);
}

// Main code
auto main() -> int {
  // Setup SDL
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0) {
    std::cerr << "Error: " << SDL_GetError() << '\n';
    return -1;
  }

  // From 2.0.18: Enable native IME.
#ifdef SDL_HINT_IME_SHOW_UI
  SDL_SetHint(SDL_HINT_IME_SHOW_UI, "1");
#endif

  // Create window with SDL_Renderer graphics context
  SDL_Window* window = SDL_CreateWindow("Dear ImGui SDL2+SDL_Renderer example",
                                        SDL_WINDOWPOS_CENTERED,
                                        SDL_WINDOWPOS_CENTERED,
                                        1280,
                                        720,
                                        SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
  if (window == nullptr) {
    std::cerr << "Error: SDL_CreateWindow(): " << SDL_GetError() << '\n';
    return -1;
  }
  SDL_Renderer* renderer =
      SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED);
  if (renderer == nullptr) {
    std::cerr << "Error: SDL_CreateRenderer(): " << SDL_GetError() << '\n';
    return -1;
  }
  // SDL_RendererInfo info;
  // SDL_GetRendererInfo(renderer, &info);
  // std::cout << "Current SDL_Renderer: " << info.name << '\n';

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImPlot::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
  io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;   // Enable Gamepad Controls

  // Setup Dear ImGui style
  // ImGui::StyleColorsDark();
  // ImGui::StyleColorsLight();
  ImGui::StyleColorsClassic();

  // Setup Platform/Renderer backends
  ImGui_ImplSDL2_InitForSDLRenderer(window, renderer);
  ImGui_ImplSDLRenderer2_Init(renderer);

  // Our state
  ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

  Linkage drawLink{
      .A        = {0, 0.1},
      .angle    = 0.0,
      .lAB      = 0.66,
      .lAD      = 1.0,
      .lBC      = 1.0,
      .lCD      = 1.0,
      .couplerN = 0.25,
      .couplerT = 0.5,
      .C_traj_x = {},
      .C_traj_y = {},
      .E_traj_x = {},
      .E_traj_y = {},
  };
  Linkage computeLink;
  float drawAngle = 0;
  float drawSpeed = 0.25;

  int nOptPoints = 3;
  std::vector<std::array<double, 2>> optPoints(nOptPoints);

  float softmin_alpha = 5.0;

  ImPlotStyle& plotStyle = ImPlot::GetStyle();

  // Main loop
  bool done       = false;
  bool threadDone = true;
  while (!done) {
    if (threadDone && computeLink != drawLink) {
      threadDone  = false;
      computeLink = drawLink;
      std::thread(computeLinkageTask, std::ref(computeLink), std::ref(threadDone)).detach();
    }
    // Poll and handle events (inputs, window resize, etc.)
    // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to
    // tell if dear imgui wants to use your inputs.
    // - When io.WantCaptureMouse is true, do not dispatch mouse input data to
    // your main application, or clear/overwrite your copy of the mouse data.
    // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input
    // data to your main application, or clear/overwrite your copy of the
    // keyboard data. Generally you may always pass all inputs to dear imgui,
    // and hide them from your application based on those two flags.
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      ImGui_ImplSDL2_ProcessEvent(&event);
      if (event.type == SDL_QUIT) done = true;
      if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE &&
          event.window.windowID == SDL_GetWindowID(window))
        done = true;
    }

    // Start the Dear ImGui frame
    ImGui_ImplSDLRenderer2_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();
    {
      const ImGuiViewport* viewport = ImGui::GetMainViewport();
      double plot_size              = 0.9 * std::min(viewport->WorkSize[0], viewport->WorkSize[1]);
      ImGui::SetNextWindowSize(ImVec2(plot_size, plot_size));  //, ImGuiCond_FirstUseEver);
      ImGui::Begin("Plot Window");
      // ImPlot::SetNextAxesToFit();
      ImPlot::SetNextAxesLimits(-1.5, 2.5, -1.0, 3.0);
      if (ImPlot::BeginPlot("My Plot", ImVec2(-1, -1))) {
        int n = std::min(computeLink.C_traj_x.size(), computeLink.C_traj_y.size());
        ImPlot::PlotLine(
            "Trajectory C", computeLink.C_traj_x.data(), computeLink.C_traj_y.data(), n);
        ImPlot::SetNextMarkerStyle(ImPlotMarker_Circle);
        ImPlot::PlotLine(
            "Trajectory E", computeLink.E_traj_x.data(), computeLink.E_traj_y.data(), n);

        ImPlot::DragPoint(optPoints.size(),
                          &drawLink.A[0],
                          &drawLink.A[1],
                          ImVec4(0.50f, 0.50f, 0.50f, 1.00f),
                          4);

        if (computeLink.C_traj_x.size() >= 360) {
          int angle          = ((int)drawAngle + 720) % 360;
          double rad         = drawAngle * M_PI / 180;
          double linkageX[4] = {drawLink.A[0],
                                drawLink.A[0] + cos(rad) * drawLink.lAB,
                                computeLink.C_traj_x[angle],
                                drawLink.A[0] + cos(drawLink.angle) * drawLink.lAD};
          double linkageY[4] = {drawLink.A[1],
                                drawLink.A[1] + sin(rad) * drawLink.lAB,
                                computeLink.C_traj_y[angle],
                                drawLink.A[1] + sin(drawLink.angle) * drawLink.lAD};

          double couplerX[3] = {drawLink.A[0] + cos(rad) * drawLink.lAB,
                                computeLink.C_traj_x[angle],
                                computeLink.E_traj_x[angle]};
          double couplerY[3] = {drawLink.A[1] + sin(rad) * drawLink.lAB,
                                computeLink.C_traj_y[angle],
                                computeLink.E_traj_y[angle]};

          plotStyle.LineWeight = 2.5;
          ImPlot::SetNextMarkerStyle(ImPlotMarker_Circle);
          ImPlot::PlotLine("Coupler", couplerX, couplerY, 3, ImPlotLineFlags_Loop);
          ImPlot::SetNextMarkerStyle(ImPlotMarker_Circle);
          ImPlot::PlotLine("Linkage", linkageX, linkageY, 4, ImPlotLineFlags_Loop);
          plotStyle.LineWeight = 1.0;
        }

        // Optimization points
        for (std::size_t i = 0; i < optPoints.size(); i++) {
          ImPlot::DragPoint(
              i, &optPoints[i][0], &optPoints[i][1], ImVec4(0.50f, 0.50f, 0.50f, 1.00f), 4);
        }

        ImPlot::EndPlot();
      }
      ImGui::End();
    }
    {
      ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiCond_FirstUseEver);
      ImGui::SetNextWindowSize(ImVec2(400, -1), ImGuiCond_FirstUseEver);
      ImGui::Begin("Linkage Customization");

      ImGui::Text("This is some useful text.");
      double f64_zero = 0.0;
      double f64_one  = 1.0;
      // ImGui::SliderFloat2("(Ax, Ay)", drawLink.A, 0.0, 1.0);
      ImGui::SliderScalarN(
          "(Ax, Ay)", ImGuiDataType_Double, drawLink.A.data(), 2, &f64_zero, &f64_one);
      ImGui::SliderFloat("angle", &drawLink.angle, -3.1416, 3.1416);
      ImGui::Separator();
      ImGui::SliderFloat("lAB", &drawLink.lAB, 0.0, 2.0);
      ImGui::SliderFloat("lBC", &drawLink.lBC, 0.0, 2.0);
      ImGui::SliderFloat("lCD", &drawLink.lCD, 0.0, 2.0);
      ImGui::SliderFloat("lAD", &drawLink.lAD, 0.0, 2.0);
      ImGui::Separator();
      ImGui::SliderFloat("coupler_t", &drawLink.couplerT, -2.0, 2.0);
      ImGui::SliderFloat("coupler_n", &drawLink.couplerN, -2.0, 2.0);
      ImGui::Separator();

      ImGui::SliderFloat("draw angle", &drawAngle, -720, 720);
      ImGui::SliderFloat("draw speed", &drawSpeed, -4.0, 4.0);

      ImGui::Separator();

      if (ImGui::SliderInt("Optimization Points", &nOptPoints, 1, 10)) {
        optPoints.resize(nOptPoints);
      }
      ImGui::SliderFloat("SoftMin alpha", &softmin_alpha, 0.01, 100);

      // ImGui::Text(" ");
      ImGui::Dummy(ImVec2(0.0f, 20.0f));

      ImVec2 window_size = ImGui::GetWindowSize();
      ImGui::Text(
          "Application average %.3f ms/frame (%.1f FPS)", 1000.0f / io.Framerate, io.Framerate);
      ImGui::Text("Window size: (%.1f, %.1f)", window_size[0], window_size[1]);
      ImGui::Text("Traj length: %zu", drawLink.C_traj_x.size());

      double T1 = drawLink.lAD + drawLink.lBC - drawLink.lAB - drawLink.lCD;
      double T2 = drawLink.lCD + drawLink.lAD - drawLink.lAB - drawLink.lBC;
      double T3 = drawLink.lCD + drawLink.lBC - drawLink.lAB - drawLink.lAD;

      ImGui::Text("T1: %.2f", T1);
      ImGui::Text("T2: %.2f", T2);
      ImGui::Text("T3: %.2f", T3);

      ImGui::Separator();

      double objFun = eval_f<double>(computeLink, optPoints, softmin_alpha);
      ImGui::Text("Objective: %f", objFun);

      ImGui::Separator();

      static int colorScheme = 0;
      ImGui::RadioButton("Dark", &colorScheme, 0);
      ImGui::SameLine();
      ImGui::RadioButton("Light", &colorScheme, 1);
      if (colorScheme == 0) {
        ImGui::StyleColorsDark();
      } else if (colorScheme == 1) {
        ImGui::StyleColorsLight();
      } else {
        ImGui::StyleColorsClassic();
      }
      static bool showImPlotDemo = false;
      static bool showImGuiDemo  = false;

      ImGui::Checkbox("ImPlot Demo", &showImPlotDemo);
      ImGui::SameLine();
      ImGui::Checkbox("ImGui Demo", &showImGuiDemo);

      if (showImPlotDemo) { ImPlot::ShowDemoWindow(); }
      if (showImGuiDemo) { ImGui::ShowDemoWindow(); }

      static bool animateLinkage = false;
      ImGui::Checkbox("Animate Linkage", &animateLinkage);
      if (animateLinkage) {
        drawAngle += drawSpeed;
        if (drawAngle > 720) { drawAngle = -720; }
        if (drawAngle < -720) { drawAngle = 720; }
      }

      if (ImGui::Button("Optimize")) {
        const auto opt_linkage = find_optimum(optPoints);
        if (!opt_linkage.has_value()) {
          std::cerr << "Could not find global optimum.\n";
        } else {
          drawLink = *opt_linkage;
        }
      }

      ImGui::End();
    }
    // ImGui::ShowDemoWindow();

    // Rendering
    ImGui::Render();
    SDL_RenderSetScale(renderer, io.DisplayFramebufferScale.x, io.DisplayFramebufferScale.y);
    SDL_SetRenderDrawColor(renderer,
                           (Uint8)(clear_color.x * 255),
                           (Uint8)(clear_color.y * 255),
                           (Uint8)(clear_color.z * 255),
                           (Uint8)(clear_color.w * 255));
    SDL_RenderClear(renderer);
    ImGui_ImplSDLRenderer2_RenderDrawData(ImGui::GetDrawData(), renderer);
    SDL_RenderPresent(renderer);
  }

  // Cleanup
  ImGui_ImplSDLRenderer2_Shutdown();
  ImGui_ImplSDL2_Shutdown();
  ImPlot::DestroyContext();
  ImGui::DestroyContext();

  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return 0;
}
